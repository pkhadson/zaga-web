var countItens = $('.slider-depoimentos .item').length;
var counterSlider = 1;
$('.slider-depoimentos .item').animate({opacity:0},0);
$('.slider-depoimentos .item:nth-child(1)').animate({opacity:1},0);
setInterval(()=>{
	if($('.slider-depoimentos:hover').length>0){return 0;}
	counterSlider++;
	$('.slider-depoimentos .item').animate({opacity:0},500);
	$('.slider-depoimentos .item:nth-child('+counterSlider+')').animate({opacity:1},500);
	if(countItens==counterSlider){counterSlider=0;}
},4000);
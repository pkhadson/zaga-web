var portfolio = {
	data: [],
	build: (data,min,max)=>{
		$('#btnSee').attr('data-see',max);
		var container = $('#portfolio .content-projects'),
		returnHtml = '';
		data.forEach((a,b)=>{
			if(b<min || b>max){return 0;}
			returnHtml+='<div class="item" data-id="'+b+'"><div class="image openModal" style="background: url('+a.image+') center top;background-size: cover;"><div class="title titleinner"><span>'+a.title+'</span></div></div><div class="info"><div class=" openModal base-title"><div class="title titleout"><span>'+a.title+'</span></div></div><div class="info-line"><div class="key">TAGS</div><div class="value">'+a.tags+'</div></div><div class="info-line '+(!a.liveBool?'disablelive':'')+'"><div class="key">LIVE</div><div class="value"><a target="_blank" href="'+a.live+'">'+a.liveText+'</a></div></div></div></div>';
		});

		container.append(returnHtml);
		$('.openModal').on('click', (e)=>{
			var th = $(e.currentTarget);
			var id = th.closest('.item').data('id');
			portfolio.showModal(id);
		});
	},
	showModal: (a)=>{
		var data = portfolio.data[a];
		var tags = '';
		$('.modal-portf').fadeIn(500);
		$('.modal-portf .modal-content').show().css('margin-top','-50%').css('opacity',0);
		$('.modal-portf .modal-content').animate({
			marginTop: 0,
			opacity: 1 
		},500);
		data.images.forEach((a,b)=>{
			tags+="<div class=\"imagee\" data-image=\""+a+"\" style=\"background: url("+a+")center top;background-size: cover\"></div>";
		});
		$('.modal-portf .title').html(data.title);
		$('.modal-portf .tags').html(data.tags);
		$('.modal-portf .scroll-images .content').html(tags);
		$('.modal-portf .scroll-images .content').css('min-width',(data.images.length*110)+'px');
		$('.modal-portf .close').on('click', portfolio.hideModal);
		$('.modal-portf .scroll-images .content .imagee').on('click', (e)=>{
			$('.modal-portf .scroll-images .content .imagee').removeClass('active');
			$(e.currentTarget).addClass('active');
			$('.modal-portf .base-image')
			.css("background",'url('+$(e.currentTarget).data('image')+') center no-repeat')
			.css('background-size','contain');
		});
		$('.modal-portf .scroll-images .content .imagee:nth-child(1)').trigger('click');
	},
	hideModal: ()=>{$('.modal-portf').fadeOut();$('body').css('overflow','auto');return 8558;}
}
$.ajax({
	url: 'assets/data/port.json',
	success: function (result) {
		portfolio.data = result;
		portfolio.build(portfolio.data,0,5);
	},
	async: false
});
$(document).mouseup((e)=> {
	var container = $(".modal-content");
	if (!container.is(e.target) && container.has(e.target).length === 0){
		portfolio.hideModal();
	}
});

$('#btnSee').on('click', (e)=>{
	var min = parseInt($(e.currentTarget).attr('data-see'));
	var max = min + 5;
	portfolio.build(portfolio.data,min,max);
});
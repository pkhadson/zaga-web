'use strict';

var gulp = require('gulp'),
sass = require('gulp-sass'),
livereload = require('gulp-livereload'),
sourcemaps   = require('gulp-sourcemaps'),
uglify       = require('gulp-uglifyes'),
plumber      = require('gulp-plumber'),
rename       = require("gulp-rename"),
fs           = require('fs'),
path         = require('path'),
concat       = require('gulp-concat');


function getFolders(dir) {
    return fs.readdirSync(dir)
      .filter(function(file) {
        return fs.statSync(path.join(dir, file)).isDirectory();
      });
}


gulp.task('sass', function () {
  return gulp.src('./assets/sass/**/*.scss')
    .pipe(sass({
  errLogToConsole: true,
  outputStyle: 'compressed'
}).on('error', sass.logError))
    .pipe(rename({ suffix: '.min' }))
    .pipe(gulp.dest('./assets/css'))
    .pipe(livereload());
});

gulp.task('asass', function () {
  livereload.listen();
  gulp.watch('./assets/sass/**/*.scss', ['sass']);
});

gulp.task('ascripts', function() {
    gulp.src('assets/js/scripts/**/*.js')
    .pipe(plumber())
    .pipe(uglify({ 
       mangle: false, 
       ecma: 6 
    }))
    .pipe(concat('scripts.min.js'))
    .pipe(gulp.dest('assets/js'))
    .pipe(livereload());
});
gulp.task('avendors', function() {
    gulp.src('assets/js/vendors/**/*.js')
    .pipe(plumber())
    .pipe(uglify({ 
       mangle: false, 
       ecma: 6 
    }))
    .pipe(concat('vendors.min.js'))
    .pipe(gulp.dest('assets/js'))
    .pipe(livereload());
});

gulp.task('scripts', function(){
      var server = livereload();
    livereload.listen();
  gulp.watch('assets/js/scripts/**/*.js', ['ascripts']);
});
gulp.task('vendors', function(){
  gulp.watch('assets/js/vendors/**/*.js', ['avendors']);
});